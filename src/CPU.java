public class CPU {
    double price;


    public class Processor{

        public Processor(String manufacturer, double cores){
            this.manufacturer = manufacturer;
            this.cores = cores;
        }
        double cores;
        String manufacturer;

        public double getCores() {
            return cores;
        }

        public String getManufacturer() {
            return manufacturer;
        }
    }

    protected class RAM{

        public RAM(double memory, double clockSpeed,  String manufacturer){
            this.memory = memory;
            this.clockSpeed = clockSpeed;
            this.manufacturer = manufacturer;
        }
        double memory;
        double clockSpeed;
        String manufacturer;

        public String getManufacturer() {
            return manufacturer;
        }

        public double getClockSpeed() {
            return clockSpeed;
        }

        public double getMemory() {
            return memory;
        }
    }

}
